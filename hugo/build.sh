#!/usr/bin/env bash
cd "$(dirname "$0")"
rm -rf ../public
echo "Building..."
hugo
echo "Updating /public..."
mv ./public ../public
git add ../public
git commit -m"update /public" ../public