---
title: ""
omit_header_text: true
date: 2019-12-26T08:47:11+01:00
draft: false
featured_image: "images/working-around-desk.jpg"
type: page
menu:
  main:
    name: Main
    weight: 1
--- 


## Who I am

I am Mike Mugge, a professional software developer and consultant. I have been developing professional solutions for
companies for over 20 years. I've worked in various industries, but more recently, I've spent some time in the legal
and real estate industries. In my career, I've worked both as an employee and as an independent contractor. For the past
4 years I have been living in Idaho and working remotely on various projects, both in state and out of state.

My online profiles can be found here:

 - [Linked-In](http://linkedin.com/in/michaelmugge)
 - [GitLab](https://gitlab.com/sylnsr)
 - [GitHub](https://github.com/idazco)
 - [StackOverflow](https://stackoverflow.com/users/390722)

Currently 100% of my development is done in a Linux environment and I focus on leveraging AWS for solution development
and hosting. My IDE of choice is Intelli-J Ultimate Edition.

---

## What I do

I specialize in providing software consulting services for complex business projects that are usually good candidates
for solution development with some ERP framework. I can handle every aspect of a project from initial design, through
development and deployment, all the way to hosting and ongoing monitoring and support.

I excel in using reliable, free open source software to deliver effective solutions that will not break your project
budget. COTS solutions are often part of the equation, but that choice is ultimately at your discretion.

I have spent my fair share of time working in start-ups and I'm comfortable with filling multiple roles at the same time.

---

## Scope of experience

#### Software Solution Development

 - Backend development and micro-services architecture
 - Integrations (ETL)
 - Reporting (BI)
 - User interface development
 - Odoo implementation and ERP consulting

#### Hosting and DevOps

 - AWS hosting and support
 - CI/CD with Git, Jenkins, Ansible, Docker and Terraform
 - Infrastructure support and monitoring

#### Management

 - Start-up support
 - Team development
 - Product and project management
