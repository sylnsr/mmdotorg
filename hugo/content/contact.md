---
title: "Contact"
omit_header_text: true
date: 2019-12-26T08:47:11+01:00
featured_image: "images/contact.jpg"
draft: false
menu:
  main:
    weight: 40
    name: Contact
---

### Linked-In !

---

It's best to contact me through Linked-In. This way I can have messages screened and see who it is that is trying to
contact me. The link to my profile is: http://linkedin.com/in/michaelmugge

Thank you for taking the time to make contact,

Cordially,  
Mike Mugge