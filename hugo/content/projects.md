---
title: "Latest Portfolio Projects"
date: 2019-12-26T08:47:11+01:00
description: "Most of our projects are conducted under strict NDAs, and cannot
be listed here, however there are some that we contribute to that are open-source
or are entirely our own."
featured_image: "images/projects.jpg"
draft: false
menu:
main:
weight: 1
name: Projects
---

## FreeMatters.org

- https://www.freematters.org
- https://gitlab.com/idazco-foss/freematters

(This is a project in its infancy and will be developing rapidly over the coming weeks)

FreeMatters.org is an open source software project which is specifically being developed to be used by law firms
to run their practice. Its based on Odoo which is also an open source software system and its Community Edition
is completely free to use. Odoo is already capable of providing the rudimentary functions needed to run a law firm,
but the FreeMatters.org project aims to provide a specialized implementation, complete with a custom user interface
that is much simpler and intuitive for attorneys to use.

IDAZCO provides consulting for law firms that need help implementing the Free Matters suite or just Odoo.
We also provide hosting and support contracts.

Additionally we provide integrations for importing data into a law firm's FreeMatters/Odoo deployment
to save attorneys time and errors from manually entering data from external systems, to the matter
management system.

Law firms that do not want to move their business onto Free Matters can still benefit from the project
when we develop integrations for them, since we use Free Matters for data warehousing and ETL rule management.

---

## Odoo In Vue

https://gitlab.com/sylnsr/odoo-in-vue

This is a prototype project for developing a Vue based SPA with an Odoo back-end. Other apps that are developed using
the Vue + Odoo combination will have all major features prototyped here first. This is a free open source project.
Anyone can use this as a basis for their own Vue app with an Odoo back-end. It uses the Quasar framework on top of Vue.

--- 

## Status Tracker

Prototype source code: https://gitlab.com/sylnsr/gostatrak

This project was developed entirely by me. Its a very simple project and is not based on Odoo.
It' simply a status tracking system and is typically used to replace plastic/metal medical status
flags used in hospitals, clinics, veterinary offices and other types of medical service facilities.

The project for the new S.P.A. has been open sourced and [published on Gitlab](https://gitlab.com/idazco-foss/statusflags-ui)

This project used to be promoted at statusflags.com as a stand-alone for profile business, but we have since moved it's
public page to [Facebook](https://www.facebook.com/statusflags) and made all the source code available for free.


##### Other Links:

https://www.facebook.com/statusflags

---

## Laser Cut Quilts

https://lasercutquilts.com/

IDAZCO worked in conjunction with OdooGap.com to provide Odoo customization services to this client.
Laser Cut Quilts is a laser cut kit wholesaler that makes effective use of Odoo's manufacturing management modules to
manage their process from inventory tracking to manufacturing and finally, to fulfilment. Laser Cust Quilts was an
exceptional client and we thoroughly enjoyed working with them.

---

# Projects in incubation

---

## Pravetro

Pravetro is planned to be a solution based on Odoo and used in the A/V Rental and event production/management industry.
Pravetro can be used for end-to-end show production and rental asset management as well as virtual trade shows and fairs.
