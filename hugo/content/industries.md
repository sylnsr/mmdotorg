---
title: "Industries"
date: 2019-12-26T08:47:11+01:00
description: "Since I like to utilize the very flexible Odoo ERP platform, I am able to cater
to any industry, however here are some specific industries that I've either recently worked in or enjoyed the most."
featured_image: "images/industries.jpg"
draft: false
type: page
menu:
  main:
    weight: 20
    name: Industries
---

## Legal

I am currently working on the open source [FreeMatters.org](http://www.freematters.org) project, which will be a
free open source portfolio project aimed at helping law firms implement the Odoo CE platform to run their entire practice.
The Odoo modules involved are already existing, free open source code and maintained by the Odoo open source community.
All that is needed is Odoo Community Edition, a few of these modules, UI customization and some training.

I have years of experience supporting the legal industry and companies that perform very transactional
legal work, such as, law firms; collection companies and title companies. All these types of businesses have the same
typical requirements that make them ideal for using Odoo, namely, management of:

 - contacts
 - projects / matters / transactions / escrows
 - tasks, task actions and task priorities
 - calendaring and appointments
 - notes and logging
 - time-sheets
 - claims, time and expense tracking
 - documents
 - vendors (accounts payable)
 - invoicing (accounts receivable)
 - allocations and disbursement

---

## Real Estate

I can support any type of real estate data management project and have extensive experience with:
 
 - real estate sales and escrow management
 - HOA management
 - property management
 
Typical clients include builders, title companies, real estate developers and HOAs.

Since supporting the real estate industry is very similar to supporting title companies, the strengths
and experience I have supporting one, lends to the other.

--- 

## Event production and A/V rentals

With years of experience supporting the event production and A/V equipment asset management in Las Vegas,
utilizing both COTS solutions and custom solutions, I now focus on leveraging Odoo Community Edition as
the basis for supporting this industry.

---

## Manufacturing

Since the core competency of Odoo is running manufacturing operations it follows that I can service all manner of
manufacturing projects. Although Odoo is suited to be implemented in manufacturing concerns of any size
I prefer to engage medium to large projects.