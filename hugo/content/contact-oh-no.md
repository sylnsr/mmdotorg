---
title: "Contact - Fail"
omit_header_text: true
date: 2019-12-26T08:47:11+01:00
featured_image: "images/contact.jpg"
draft: false
---

Oh-no! It appears something went wrong.

OK, well just email us at info@idaz.co