---
date: 2020-06-05T10:58:08-04:00
tags: ["asterisk", "dictate", "convert", "raw", "wav"]
description: "Convert Asterisk dictation .raw files to .wav"
title: "Convert Asterisk dictation .raw files to .wav"
---

# Convert Asterisk dictation .raw files to .wav 

I recently implemented an Asterisk solution and made use of the Dictate application which creates .raw audio files of 
the Dictations. These files need to be encoded to wav format even though specifying in FreePBX, that the output of 
dictated files should be WAV files. When researching how to do the conversion I did find various examples of using the 
sox app to convert these files, however none of these examples worked exactly as documented. Some documentation was 
incomplete and others were outdated an often, the forums which raised the question, went unanswered.

For the version of FreePBX which I am using, I found the following to command sequence to work:

    sox -t auto -w -s -r 8000 -c 1 {input_file}  {output_file}

An excellent way to increase the volume, is to add the -v option with a multiplier to increase the volume by. This is 
especially helpful in our case since we have a few doctors that like to dictate very softly.

I hope this information is helpful to anyone else that has run into this issue. 
