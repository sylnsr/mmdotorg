---
date: 2020-06-12T00:00:00-00:00
description: "StatusFlags.com Change Log"
tags: ["statusflags.com", "changelog"]
title: "StatusFlags Change Log"
---

## 2020-06-12

- All logins must now be done at https://login.statusflags.com.
- The server and the SPA are now separate projects.
- The SPA is now an open source project [hosted on Gitlab](https://gitlab.com/idazco-foss/statusflags-ui)
- The SPA is setup for use with your own server. Just ...
   - deploy your own server with correct DNS, valid cert etc
   - deploy the SPA with your new `SERVER_URL` config

---

## 2021-07-7

- For the legacy UI, fixed a JSON parsing issue that occurs when all channels are deleted
