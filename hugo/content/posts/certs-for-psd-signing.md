---
date: 2020-06-05T10:58:08-04:00
tags: ["pdf", "singing", "certs"]
description: "Generating certs for testing PDF signing"
title: "Generating certs for testing PDF signing"
---

# Generating certs for testing PDF signing 

Run the following as root:

    openssl genrsa -out ca.key 2048
    openssl req -new -key ca.key -out ca.csr
    openssl x509 -req -days 365 -in ca.csr -signkey ca.key -out ca.crt
    openssl pkcs12 -export -in ca.crt -inkey ca.key -name ca -passout pass:password -out ca.pfx
