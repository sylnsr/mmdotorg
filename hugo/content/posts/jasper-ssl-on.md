---
date: 2020-06-05T10:58:08-04:00
description: "Connect JasperServer to a Postgres Data Source with SSL"
tags: ["golang", "postgres"]
title: "Jasper Server SSL connection"
---

# Connect JasperServer to a Postgres Data Source with SSL

In response to question such this:

    https://community.jaspersoft.com/ireport-designer/issues/4135


In the JDBC connection string, just add this:

    ?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory

i.e. just setting ssl=true is not enough.


And just to state the obvious, your Postgres server must support SSL connections.
