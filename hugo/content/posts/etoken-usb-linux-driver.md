---
date: 2020-06-05T10:58:08-04:00
tags: ["etoken", "usb", "linux", "driver"]
description: "Installing Linux 64bit driver for eToken USB"
title: "Installing Linux 64bit driver for eToken USB"
---

# Installing Linux 64bit driver for eToken USB 

If needed install, opensc and pcsc-lite

    yum install opensc pcsc-lite

Download the driver, unzip and install

    wget http://download.fh-swf.de/dvz/software/eToken/Linux/Software/SafeNetAuthenticationClient_Linux_8_1.1.zip
    unzip SafeNetAuthenticationClient_Linux_8_1.1.zip
    cd SAC\ 8.1\ Linux/x86_64/
    unzip SAC_8_1_0_4_Linux_RPM_64.zip
    cd SAC_8_1_0_4_Linux_RPM_64/Signed\ installation\ scripts/
    chmod +x signed-install_SafenetAuthenticationClient-8.1.0-4.x86_64.rpm.sh
    cp ../RPM/SafenetAuthenticationClient-8.1.0-4.x86_64.rpm .
    ./signed-install_SafenetAuthenticationClient-8.1.0-4.x86_64.rpm.sh

You should see the following (choose 1 for English):

    Searching SafenetAuthenticationClient-8.1.0-4.x86_64.rpm... OK
    Searching RPM-GPG-KEY-SafenetAuthenticationClient... OK
    Deleting existing key
    Importing key: RPM-GPG-KEY-SafenetAuthenticationClient
    Starting installation
    ########################################### [100%]
    ########################################### [100%]
    Adding Token security provider......done.
    Starting PC/SC smart card daemon (pcscd): [  OK  ]
    SafeNet Authentication Client installation completed.
    Choose Client Language:
    ------------------------
    1. English.....(En)
    2. Spanish.....(Es)
    3. French......(Fr)
    4. Italian.....(It)
    5. Japanese....(Jp)
    6. Korean......(Ko)
    7. Russian.....(Ru)
    8. Chinese.....(Zh)
    9. Portoguese..(Pt)
    10. Thai.......(Th)
    Your choice [1-10] -->1
    Installing Language code En.
    Done!

Check

    opensc-tool --list-readers

The result should be similar to:

    # Detected readers (pcsc)
    Nr.  Card  Features  Name
    0    Yes             AKS ifdh 00 00

Find out which slot the device is in:

    pkcs11-tool --module /usr/lib64/libeToken.so -L

You should see something to the effect of:

    Available slots:
    Slot 0 (0x0): AKS xxxx 00 00
    token label:   my label
    token manuf:   SafeNet, Inc.
    token model:   eToken
    token flags:   rng, login required, PIN initialized, token initialized, other flags=0x200
    serial num  :  xxxxxxx
    Slot 1 (0x1):
    (empty)
    Slot 2 (0x2):
    (empty)
    Slot 3 (0x3):
    (empty)
    Slot 4 (0x4):
    (empty)
    Slot 5 (0x5):
    (empty)

----

Acknowledgements:

"user4668" for showing how to see the used slots http://bit.ly/1w2dUT8


Other Sources for the SAC:

http://www.digicert.com/util/SafeNetAuthenticationClient-610-011815-002_SAC_Linux_v8.1.zip
http://www.digicert.com/util/SafeNetAuthenticationClient.8.2.27.0.dmg 
