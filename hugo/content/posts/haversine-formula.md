---
date: 2020-06-05T10:58:08-04:00
description: "Find the nearest location by latitude and longitude in MySQL using the Haversine formula"
tags: ["MySQL", "Haversine"]
title: "Find the nearest location by latitude and longitude in MySQL using the Haversine formula"
---

# Find the nearest location by latitude and longitude in MySQL using the Haversine formula

If you need to find the nearest location of any point of interest by latitude and longitude in MySQL, 
then you can use the Haversine formula. Following here is and example that assumes you have a table named "destinations"
which contains amongst other things, the latitude and longitude of those 'destinations'. An example of valid destinations
in a real-world application would be a table listing hotels and their lat/long coordinates which would enable one to write
a query to return the nearest hotels from that table based on your lat/long coordinates. (The distance provided is in miles.
To get the distance in kilometers, exchange 3539 with 6371)

```sql
SET @my_lat = 35.229205 ; -- the source latitude
SET @my_lon = -114.26811 ;-- the source longitude

SELECT *, ( 3959 * acos( cos( radians(
-- latitude
@my_lat) ) * cos( radians( 
destination.latitude ) ) * cos( radians( 
destination.longitude ) - radians(
-- longitude
@my_lon) ) + sin( radians(
-- latitude
@my_lat) ) * sin( radians( 
destination.latitude ) ) ) ) AS distance 
-- table containing targets to compare distance
FROM destination
ORDER BY distance LIMIT 1 ;

```

Full example at SQL Fiddle ---> http://sqlfiddle.com/#!2/abba1/4/0 ( PLEASE DONATE )

I recently needed to use this to find the official timezone for a city closest to a person based on their latitude, longitude coordinates in MySQL, so as not to have to depend on look-ups to a web service. For anyone interested, city-timezone data can be downloaded from the geonames.org website and loaded as the 'destinations' table.

Reference:
http://stackoverflow.com/questions/574691/mysql-great-circle-distance-haversine-formula
