---
date: 2020-07-30T10:58:08-04:00
description: "Odoo In MicroK8s"
tags: ["odoo", "kubernetes", "microk8s"]
title: "Odoo In MicroK8s"
---

# Odoo In MicroK8s

---

This post provides a simple guide for anyone wanting to get Odoo running on Kubernetes quickly for development or review
purposes. Since most Odoo developers I know, including myself, use some sort of Debian flavor of Linux, I've selected
MicroK8s for the Kubernetes environment.

It's obviously possible to get it going with a set of self written Kubernetes manifests, however, I've found best and
fastest way to get the job done is with Helm and the Bitnami Odoo Chart.

I have gotten everything in this post to work with MicroK8s but there's no reason why it should not work in any other
Kubernetes environment. Due to certain Kubernetes limitations with dynamic local storage provisioning, your host machine,
which is also your only node, should be setup as an NFS server. If you already have an NFS server available on your
network, you can skip that step. This setup also facilitates a simpler migration to a production environment where you
will likely be using NFS as well. For example, AWS where your EC2 instances would connect to EFS resources.

---

## Step #1: Install the MicroK8s snap

#### Level: easy

 - Follow the steps on the MicroK8s site to install Kubernetes: https://microk8s.io/docs
   - Skip the step for creating an alias for the kubectl command for now
 - Create a command alias for microk8s.kubectl: `sudo snap alias microk8s.kubectl kubectl`
 - Note that since we will be using NFS, you will not need to install the `storage` add-on
 - Enable the Helm 3 add-on: `microk8s enable helm3`
 - Create a command alias for microk8s.helm: `sudo snap alias microk8s.helm helm`

---

## Step #2: Setup your NFS server

#### Level: easy

Skip this step if you already have an NFS server

 - Follow the steps on this site to setup an NFS server and directory to share, on your host:
   - https://vitux.com/install-nfs-server-and-client-on-ubuntu/
 - Add an NFS export to `/etc/exports` (changing the sub-net as needed), then restart the NFS service
   - `/var/nfs_share    10.0.1.0/24(rw,sync,no_subtree_check,no_root_squash)`

---

## Step #3: Configure the default storage provisioner for your K8s cluster

#### Level: easy

One of the requirements set for the Bitnami chart for Odoo is that you have "PV provisioner support in the underlying
infrastructure" which basically means that your K8s cluster needs to be able to dynamically provide storage for pods.
For this you can simply download and run [this manifest](/text/nfs-default-storage.yml) to setup an NFS provisioner.

**NOTE:** You MUST change line 100 and 106 to point to the IP address of your own NFS server.
Once downloaded, you simply apply the manifest with: `kubectl apply -f nfs-default-storage.yml`

When its applied you can access the Kubernetes Dashboard and make sure that the `nfs-sc` storage class is present in the
default namespace.  
 

---

## Step #4: Run the Bitnami Chart for Odoo

#### Level: intermediate

 - Add the [Bitnami Chart repo](https://github.com/bitnami/charts/tree/master/bitnami/odoo/#installing-the-chart):
 `helm repo add bitnami https://charts.bitnami.com/bitnami`
 - Run this command to install Odoo 13 with Helm:
 ```
helm install bn-odoo13 bitnami/odoo --set odooPassword=odoo,postgresql.postgresPassword=odoo,image.tag=13.0.20200810
```

Running this chart will provide extra instructions for finding the IP address of the newly deployed chart. You'll need
to wait a few minutes for everything to be setting and running. Note, your will not need to access Odoo instance over
port 8069 since the Bitnami chart will already make it available over port 80. Once you are ready to login, login with
username `user@example.com` with the password `odoo`

---
