---
date: 2020-06-05T10:58:08-04:00
description: "Setting up a dial-plan for dictations in Asterisk"
tags: ["asterisk", "dictate", "dial-plan"]
title: "Setting up a dial-plan for dictations in Asterisk"
---

# Setting up a dial-plan for dictations in Asterisk

Setting up a dictation system in FreePBX is a snap ... IF you're familiar with Asterisk and FreePBX.
I have spent 3 days toying around with it and finally setup an Asterisk based system with no prior experience.

First though, let me give out due kudos to the guys over at FreePBX.org for putting out a great product. Also check out their SipStation.com site for getting SIP trunking service. Since the creators of FreePBX are closely affiliated with SipStation.com it's no wonder that setting up your SipStation.com SIP account details into your FreePBX server is drop-dead-easy. Thank you guys!

So why was setting up an Asterisk dictation system so time consuming?

 - Well for one thing, there is a lot of documentation to go through if you are a complete noob like me. I am a firm 
   believer that one should read the documentation and ask questions if you're still stuck. The best book I have found 
   to date for Asterisk is Practical Asterisk 1.4 and 1.6

 - One of the opening quotes of that book states in part "...how much fun tinkering could be" which I whole-heartedly 
   agree with. If you're noob like am, you're going to want to play with your FreePBX system for a few days get a feel 
   of all it can do.

 - For questions that you may post on various forums, dont always expect an accurate; fast or courteous reply, but dont 
   let that discourage you from posting questions either.

 - There are a lot of loose ends that one must string together in FreePBX to accomplish a complete dictation workflow. 
   If you're new to the system, you're just going to have to get familiar with all the features and figure out what works 
   best for you scenario. The reason there are so many loose ends is because it should be that way. There is no one-size-fits-all 
   configuration for anything in Asterisk.

 - Also, I have noticed that when one asks for assistance on the forums with regard to using the Dictate feature, 
   everyone seems to asume that the users are part of the local network. If you are setting up a Asterisk dictation system 
   where the users are calling in from a PSTN then you need to make that clear when you post your questions. It does make 
   a difference when the users of your dictation system are not part of the local phone system.


That being said, my current solution simply involves having calls from an inbound route go to an IVR which I use as a 
"main menu" for the inbound calls and some other routing, until the user simply dials *34 to begin the dictation app. 
I would prefer if the dictation app started automatically, but I haven't quite figured that out yet.

The next tricky part is in the configuration of the [app-dictate-record] dialplan. Using my routing model above, the 
${AMPUSER} variable is null and hence the default extension in that dialplan cannot work as provided by the FreePBX distro. 
The intention of how they have it coded is to have the dictation go into a folder which is named after the users extension. 
Hence, if the user from extension 101 uses the dictation feature, their dictations would go into folder 101. In my case, 
since the callers from my system are inbound over the PSTN, they are not authenticated as local users, hence ${AMPUSER} is 
null and all the dictations get dumped into the same folder. Also, the Dictate app asks the users to enter a file name, 
which is a really really bad idea for my solution, because if a user uses a filename they have used before, Dictate will 
over-write the existing one - YIKES!

To solve all these issues, I over-write the existing dictation dialplan with this one:


    [app-dictate-record]
    
    exten => *34,1,Answer
    exten => *34,n,Macro(user-callerid,)
    exten => *34,n,Noop(CallerID is ${AMPUSER})
    exten => *34,n,Set(DICTENABLED=${DB(AMPUSER/${AMPUSER}/dictate/enabled)})
    exten => *34,n,Set(CIDPRE=${CALLERID(name)})
    exten => *34,n,Set(CIDPRE=${CIDPRE:0:2})
    exten => *34,n,GotoIf($[$["x${DICTENABLED}"="x"]|$["x${DICTENABLED}"="xdisabled"]]?nodict:dictok)
    exten => *34,n(nodict),Playback(feature-not-avail-line)
    exten => *34,n,Hangup
    exten => *34,n(dictok),Dictate(/var/lib/asterisk/sounds/dictate/${CIDPRE},${STRFTIME(${EPOCH},,%Y%m%d-%H:%M:%S)})
    exten => *34,n,Macro(hangupcall,)

I use the CIDPRE variable to capture the "CID Prefix" configured elsewhere in my routing model, and that corresponds 
with a specific user as selected through the IVR and I use an auto-generated date-time file name so that each filename 
will be unique. One of my main goals is to do as much of the configuring through the FreePBX GUI as possible. The code 
snippet above is the only thing I had to customize under the hood.

Done, now we're ready to begin testing!

[Update]
This is what I eventually came up with:

    [app-dictate-record-tmsoa]  
    exten => _.,1,Answer  
    ;exten => _.,n,SayDigits(1.2)  
    exten => _.,n,Macro(user-callerid,)  
    exten => _.,n,Set(CIDPRE=${CALLERID(name)})          ;capture the CID info  
    exten => _.,n,Set(CIDPRE=${CIDPRE:0:3})          ;get the first 3 characters of the CID prefix  
    ;set a file name that matches the date + time and the and then the paths  
    exten => _.,n,Set(DICTFILENAME=${STRFTIME(${EPOCH},,%y%m%d-%H.%M.%S)})  
    exten => _.,n,Set(DICTFILEPATH=/var/spool/asterisk/dictate)  
    exten => _.,n,Set(DICTFILEPATHRAW=${DICTFILEPATH}/raw/${CIDPRE})  
    exten => _.,n,Set(DICTFILEPATHWAV=${DICTFILEPATH}/wav/${CIDPRE})  
    ;make sure the audio file directories exist  
    exten => _.,n,System(mkdir -p ${DICTFILEPATHRAW})  
    exten => _.,n,System(mkdir -p ${DICTFILEPATHWAV})  
    exten => _.,n,System(mkdir -p ${DICTFILEPATHRAW}/.archive)  
    ;authenticate with VM pwd in MB matching CID prefix  
    exten => _.,n,VMAuthenticate(${CIDPRE}@default)  
    ;record dictation to folder matching CID prefix with the file name in date-time format  
    exten => _.,n,Dictate(${DICTFILEPATHRAW},${DICTFILENAME})  
    ;** DO NOT ADD ANYTHING AFTER THIS DIAL-PLAN SINCE IT  
    ;** WILL JUST BE IGNORED IF THE CALL IS DROPPED/HUNGUP

Note:
I use the "SayDigits(1.2)" on development hosts to indicate the version number for dial plans I am working on. 
This gets commented out befor deployment.
