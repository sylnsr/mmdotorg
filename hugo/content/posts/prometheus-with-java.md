---
date: 2015-12-15T10:58:08-04:00
description: "Using Prometheus with Java Jersey"
tags: ["python", "AWS lambda", "SES", "email"]
title: "Prometheus with Java"
---

# Using Prometheus with Java in a Jersey project 

---

Step 1, add the dependency to your project. If you're using maven, add the dependency:

    <dependency>
        <groupId>io.prometheus</groupId>
        <artifactId>simpleclient_servlet</artifactId>
        <version>0.0.6</version>
    </dependency>

---

Step 2, create a class and register some metrics. Here I have a summary and some counters:

    public class Metrics {
    
        public static final Summary requestLatency = Summary.build()
                .name("requests_latency_seconds")
                .help("Request latency in seconds.").register();
    
        public static final Counter requestFailures = Counter.build()
                .name("requests_failures_total")
                .help("Request failures.").register();
    
        public static final Counter requestsTotal = Counter.build()
                .name("requests_total")
                .help("Request.").register();
    
        public static final Counter uploadedFilesSucceeded = Counter.build()
                .name("upload_file_success")
                .help("Total files uploaded to S3.").register();
    
        public static final Counter tmpFilesNotCleared = Counter.build()
                .name("temp_files_not_cleared")
                .help("Total files that could not be removed from cache").register();
    
    }

---

Step 3, set a class level variable in your main class to instantiate your Metrics class:

    private final Metrics metrics = new Metrics();

---

Step 4, manipulate your metrics in your code as needed. For example you need to call the inc() function on your counters to increment them. You can add to the requestLatency metric as follows:

    @GET
    @Path("/something")
    @Produces(MediaType.TEXT_PLAIN)
    public String Something() {
        try {
            Summary.Timer timer = Metrics.requestLatency.startTimer();
            // do some work ...
            timer.observeDuration();
        } catch (Exception e) {
            return e.getMessage();
        }
        return "It works!";
    }

---

Step 5, add a web method to dish out your metrics. This is the part that I did not like from other online examples (e.g. http://www.boxever.com/easy-java-instrumentation-with-prometheus) ... since they use the Metrics Servlet provided by Prometheus and all that does is user the writer to gather the data for you, so you need to mess with registering and deploying a new servlet within your code. TOO MUCH HASSLE. Just do what the Prometheus servlet does. Load the registry and use the TextFormat class to get your metrics. No need to deploy a servlet! :

    @GET
    @Path("/metrics")
    @Produces(MediaType.TEXT_PLAIN)
    public String Metrics() {
        StringWriter writer = new StringWriter();
        try {
            io.prometheus.client.exporter.common.TextFormat.write004(
                    writer, CollectorRegistry.defaultRegistry.metricFamilySamples());
        } catch (Exception e) {
            return e.getMessage();
        }
        return writer.toString();
    }


Done!

---

Here is some sample output from one of our apps using this technique:

    # HELP requests_failures_total Request failures.
    # TYPE requests_failures_total counter
    requests_failures_total 0.0
    # HELP temp_files_not_cleared Total files that could not be removed from cache
    # TYPE temp_files_not_cleared counter
    temp_files_not_cleared 0.0
    # HELP requests_latency_seconds Request latency in seconds.
    # TYPE requests_latency_seconds summary
    requests_latency_seconds_count 0.25
    requests_latency_seconds_sum 1.0
    # HELP upload_file_success Total file uploaded to S3.
    # TYPE upload_file_success counter
    upload_file_success 25874588.0
    # HELP requests_failures_total Request failures.
    # TYPE requests_failures_total counter
    requests_failures_total 2.0
