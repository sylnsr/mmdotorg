---
date: 2020-06-30T10:58:08-04:00
description: "Odoo Development Tips"
tags: ["odoo"]
title: "Odoo Dev Tips"
---

# Odoo Development Tips

---

## Development Setup

Assumptions:
 - You are a Python developer
 - You have at least basic Odoo development knowledge
 - You are using Ubuntu 18.04 for development (or similar)

Pre-requisites:

 - Python setuptools: `sudo apt-get install python3-setuptools`
 - Python wheel: `pip3 install wheel`
 

 1. Get Odoo one of 2 ways (choose just one, unless you know what you are doing):
    - **Option 1**: Clone the Odoo source code to a directory of your choice. You can use the following script to do this.
    Be sure to read the [source code](https://gitlab.com/sylnsr/linux-stuff/-/raw/master/dev/scripts/odoo/thin-clone-odoo.sh)
    if you have security concerns. Just run the this command in a directory where you want to download the source code
    (be sure to choose the correct version of Odoo for you when prompted):    
    
    ```source <(curl -s https://gitlab.com/sylnsr/linux-stuff/-/raw/master/dev/scripts/odoo/thin-clone-odoo.sh)```
    - **Option 2**: Install Odoo 13 using the .deb installer.
 
 2. Setup your Python 3.6 virtual environment by running the following in the root directory of the project:  
    ```python3 -m venv ./venv```
 
 3. Activate the Python 3 virtual environment:  
    ```source ./venv/bin/activate```
 
 4. Install the Odoo requirements into the virtual environment:  
    ```pip3 install -r <path-to-odoo-source-code>/requirements.txt```
    
 5. Configure a very basic `odoo.conf` file and copy it to `/etc/odoo/odoo.conf`. Setup the dir paths so that they match
 what you have in your Odoo config file. For example, assuming you:
 
    - downloaded the source to ```~/git/odoo/13/```
    - have your local module project cloned to ```~/git/my-repo/my-project/```
    - are developing you modules in your project in a sub-dir named `addons`
    - created the dir path ```~/tmp/var_lib_odoo``` for Odoo data files
 
    ...here is a sample with default credentials for the demo database:  
    ```
    [options]
    data_dir = ~/tmp/var_lib_odoo
    addons_path = ~/git/my-repo/my-project/addons,~/git/odoo/13/addons
    admin_passwd = odoo
    
    workers = 1
    max_cron_threads = 0
    list_db = True
    db_name = demo
    db_host = localhost
    db_user = odoo
    db_password = odoo
    ```
    Note: setting workers to 1 (one) ensures that your debugger will be hooked on the only thread Odoo is running.
  
 6. Ensure your local Postgres database is running. If you want to use a Docker container for Postgres, you could just
 put this code in a bash script and run it:
     ```
    #!/usr/bin/env bash
    cd "$(dirname "$0")"
    docker run --rm -it --name dev13_pg12 \
    -e POSTGRES_PASSWORD=odoo \
    -e POSTGRES_USER=odoo \
    -e POSTGRES_DB=postgres \
    -p 5432:5432 \
    postgres:12
    ```
 
 7. Start debugging Odoo in your IDE. For example, if you have Odoo source installed at `~/git/odoo/13`, your debugger
 would be setup to start debugging this Python script: `~/git/odoo/13/odoo-bin`. Note that your debugger must also be
 setup to use the Python environment in the virtual environment.

---

### Common dev setup issues 

#### "`greenlet.error`"
If you get the following error when debugging: `greenlet.error: cannot switch to a different thread` and the long-polling
port is not working, then look at this: https://github.com/miguelgrinberg/Flask-SocketIO/issues/65#issuecomment-294375994

Beware though, enabling the "Gevent compatible" option may cause debugging to no longer work.
See https://youtrack.jetbrains.com/issue/PY-11910  


#### "`port already in use`"
If you install Odoo from the installer AND THEN you try to run it from source, you will probably get an error
indicating that port 8069 is already in use. This is because the installer starts Odoo running as a service so the
port will already be occupied. Stop the already running Odoo process.


---

### Recommended Tools

- IDE: [IntelliJ IDEA](https://www.jetbrains.com/idea/) or [Pycharm](https://www.jetbrains.com/pycharm/)
- Python Shell: [IPython](https://ipython.readthedocs.io/en/stable/install/index.html)
- [Odoo-RPC-Client](https://pypi.org/project/odoo-rpc-client/) library for Python: 

The [IPython](https://ipython.org/) shell is especially useful with [odoo-rpc-client](https://github.com/katyukha/odoo-rpc-client)
for testing your class methods that are accessible over Odoo's built-in JSON-RPC API. In other words, using these tools
together gives you a productive live sandbox environment for testing your modules over the RPC-API without having to
constantly start and stop any experimental Python scripts.
   
To quickly get started with IPython and Odoo RPC Client, just follow these steps:

 1. Activate the virtual environment:  
    ```source ~/venv/bin/activate```
 
 2. Start an IPython shell:  
    ```ipython```
    
 3. Import Odoo RPC client:  
    ```from odoo_rpc_client import Client```
    
 4. Setup the connection for Odoo RPC client:    
    ```odoo = Client(host='127.0.0.1', dbname='demo', user='admin', pwd='admin', protocol='json-rpc')```

Now you'll be in a live Python environment for interacting with your Odoo instance from IPython.

For example, retrieve the first company email from the res.partner model:

    odoo['res.partner'].browse(1).email

... which results in:
    
    Out[1]: 'info@yourcompany.example.com'

---

### Odoo with Vue

If, for whatever reason, you dont want to develop your Odoo UI solution using Odoo's native QWeb framework, but want to
use the simpler and much more ubiquitous Vue framework, then you should check out the 
[Odoo In Vue](https://gitlab.com/sylnsr/odoo-in-vue) repo.

It provides a working prototype of a Vue app that connects to Odoo in the back-end. Additionally, because creating a
whole lot of components from scratch is a whole lot of unnecessary work, the solution uses the Quasar framework to make
dramatically increase your development time. Quasar is also beneficial because it makes porting your Vue app to Electron
or Cordova etc, extremely simple. 

---