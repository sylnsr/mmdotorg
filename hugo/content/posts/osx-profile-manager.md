---
date: 2020-06-05T10:58:08-04:00
description: "OS X Mountain Lion Server - Profile Manager / Wiki, Start Up Errors"
tags: ["osx server", "profile manager"]
title: "OS X Mountain Lion Server - Profile Manager / Wiki, Start Up Errors"
---

## OS X Mountain Lion Server - Profile Manager / Wiki, Start Up Errors

### Errors which relate to the issue (separate error on each separate line below):

    ProfileManager[1318]: Retry #[n] of database migration...
    
    devicemgrd: Terminating on unhandled exception Rake command failed with 256 at 
    /Applications/Server.app/Contents/ServerRoot/usr/share/devicemgr/webserver/devicemgrd ...
    
    The error occurred while reading settings for the Profile Manager service.
    
    servermgr_wiki: No attempt was made to create Wiki file data store directory

### References I used to try to fix the issue from other folks having similar issues:

https://discussions.apple.com/message/23350331#23350331
https://discussions.apple.com/message/19515803#19515803
http://forums.macrumors.com/showthread.php?t=1411450
https://discussions.apple.com/message/23190851#23190851
https://discussions.apple.com/message/20334099#20334099
https://discussions.apple.com/thread/3189397?start=30&tstart=0
http://support.apple.com/kb/ht5139 (this was too drastic in my opinion to even consider)

### The real problem:

I used to have my Server Service Data on an external volume mounted at /Volumes/OSXServerData. To keep the Service Data for Profile Manage, the Wiki, and indeed all server services, on another volume other than the startup volume, one simply needs to open Server.app, select the host, click Settings then click the Edit button the same line where it says "Service Data:" and shows which volume the Service Data is currently on. Please refer to this URL for more info: http://support.apple.com/kb/HT4974 -- It is important to note in that page that is says:

"Important: Do not change the name of the service data storage volume after you designate it in Server app."

For one thing, this means that you should not edit this setting, AFTER you have already removed or renamed the volume. Apple requires that you change the Service Data volume BEFORE removing the volume. For one thing, this is so that the functionality in the server app can copy the Service Data from the original location to the new volume you designate. There us a fundamental flaw to Apple's design herein. If the volume (disk) has crapped out you have little choice, but to specify a new volume without "change[ing] the name of the [current] service data storage volume" - that is because your crapped out drive where the service data was, is no longer accessible - obviously.

No worries. I just expect that once I specify a new volume, that Server.app will recreate the default settings in the new location. After all, the new volume does correctly show in the Settings tab after it is set. And indeed it does create the /Library/Server/ folder on the new volume .... and as expected, it is filled with all the goodies one would expect to see there. It gives the perfect appearance that it has successfully configured Server to use the new volume.

Alas, that is a lie. Keep in mind that the volume that crashed was mounted at /Volumes/OSXServerData thusly, the location of
the service data used to be /Volumes/OSXServerData/Library/Server/.Upon deeper inspection of the log files, I found the
following types of errors:

`postgres dataDir /Volumes/OSXServerData/Library/Server/PostgreSQL For Server Services/Data does not exist as directory`

.. and ..

`Configured wiki FileDataPath /Volumes/OSXServerData/Library/Server/Wiki/FileData does not exist as directory`

This basically told me that Server.app failed to reconfigure the volume path when the new Service Data volume was specified.

So what I had to do was search /Library/Server for any files containing the old volume mount point. Invariably they would be either config files, old config files which Server.app deprecated, or log files.
I used mc (Midnight Commander) to locate those files but you can use Finder to whatever you want to.
Once I obtained the list of files, I used `sudo nano /Library/Server/...` and the path to the particular configuration file,
to edit the path information which needed to be fixed.

The files you are looking for here end with .conf, .xml or .plist and are typically located in a Config sub directory.
Some examples are:

    /Library/Server/Messages/Config/jabberd/router.xml
    /Library/Server/Messages/Config/jabberd/sm.xml
    /Library/Server/Mail/Config/dovecot/conf.d/10-mail.conf
    /Library/Server/Software\ Update/Config/swupd.conf
    /Library/Server/Calendar\ and\ Contacts/Config/caldavd.plist
    /Library/Server/Mail/Config/dovecot/partition_map.conf
    /Library/Server/Wiki/Config/collabd.plist
    /Library/Server/Software\ Update/Config/swupd.plist

I simply ignored the files which has ".bak", "unusable" or "previous" in the path because these are obviously deprecated. Then I restarted the host and everything worked again. 
